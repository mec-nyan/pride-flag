import "./style.css";

const root = document.getElementById("root");

const bands = document.createElement("div");
bands.className = "bands";

root.appendChild(bands);

const colors = {
  red: "#E22016",
  orange: "#F28917",
  yellow: "#EFE524",
  green: "#78B82A",
  blue: "#2C58A4",
  purple: "#6D2380",
  black: "#000000",
  brown: "#945516",
  lightblue: "#7BCCE5",
  pink: "#F4AEC8",
  white: "#FFFFFF",
  gold: "#FDD817",
  lavender: "#66338b",
};

const bandColors = [
  colors.red,
  colors.orange,
  colors.yellow,
  colors.green,
  colors.blue,
  colors.purple,
];
for (const color of bandColors) {
  const band = document.createElement("div");
  band.style.backgroundColor = color;
  band.className = "band";
  bands.appendChild(band);
}

const squares = document.createElement("div");
squares.className = "squares";

root.appendChild(squares);

const squareColors = [
  colors.black,
  colors.brown,
  colors.lightblue,
  colors.pink,
  colors.white,
  colors.gold,
  colors.lavender,
];

const perc = Math.sqrt((100 * 100) / 2);

for (let i = 0; i < squareColors.length; ++i) {
  const side = perc + 12 * (5 - i);
  const sqr = document.createElement("div");
  sqr.style.height = side + "vh";
  sqr.style.width = side + "vh";
  if (i === squareColors.length - 1) {
    sqr.className = "ring";
    sqr.style.borderColor = squareColors[i];
  } else {
    sqr.style.backgroundColor = squareColors[i];
  }
  sqr.style.left = `${-perc / 2}vh`;
  squares.appendChild(sqr);
}
